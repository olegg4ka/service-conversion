<?php


namespace Application;

use Core\Component\Container\Container;
use Core\Component\FastRoute\FastRoute;
use Core\Component\Redis\RedisConnection;
use Core\Component\Redis\Storage\Catalog;
use Exception;

/**
 * Class Application
 * @package Core
 */
class Application
{
	private static $instances = [];

	private $routes;

	private $redis;
	/**
	 * Application constructor.
	 * @throws Exception
	 */
	protected function __construct()
	{
		$this->prepeareProjectFolders();
		$this->redisIn();
		$this->routeIn();
	}

	/**
	 * @return Application
	 */
	public static function getInstance(): Application
	{
		$class = static::class;
		if (!isset(self::$instances[$class])) {
			self::$instances[$class] = new static();
		}
		return self::$instances[$class];
	}

	/**
	 * @return void
	 */
	public function routeIn(): void
	{
		$this->routes = new FastRoute();
	}

	/**
	 * @return void
	 * @throws Exception
	 */
	public function redisIn():void
	{
		(new Catalog())->initCatalog();
	}

	/**
	 * @return void
	 */
	public function prepeareProjectFolders()
	{
		$config = Container::get('config');
		foreach ($config['folders']['animalBot'] as $folder){
			if (!is_dir($folder)) {
				mkdir($folder);
			}
		}
	}

}