<?php

namespace Application\Model\DocumentTransfer;

use Application\Model\Common\Documents\Excel\CommonExcel;
use Application\Model\DocumentTransfer\DataBaseRequest\LoadDataFromUser;
use Core\Backend\Common\Documents\SaveFiles\SaveFiles;
use Core\Backend\Component\Messenger\Telegram;
use Core\Component\Mail\Mail;
use Core\Component\Redis\Storage\Catalog;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Telegram\Bot\Exceptions\TelegramSDKException;

const Volonter = 1;

/**
 *
 */
class DocumentTransferVolonter
{
	/**
	 * @param array $data
	 * @return void
	 * @throws GuzzleException
	 * @throws Exception
	 */
	public static function saveFiles(array $data)
	{
		$file_name = SaveFiles::saveFiles($data);
		self::parseExcel($data, $file_name);
	}

	/**
	 * @param $data
	 * @param $file_name
	 * @return void
	 * @throws TelegramSDKException
	 */
	public static function parseExcel($data, $file_name)
	{
		$text = 'Дякуємо, що скористалися ботом!';
		$telegram = (new Telegram())->getTelegram();
		try {
			$catalog = (new Catalog());
			$animalType = $catalog->getCatalog('AnimalType');
			$productType = $catalog->getCatalog('ProductType');
			$measureUnits = $catalog->getCatalog('MeasureUnits');

			$names = [
				'A' => 'product',
				'B' => 'animalType',
				'C' => 'productType',
				'D' => 'notesProduct',
				'E' => 'count',
				'F' => 'odMeasurement'
			];

			$file_info = CommonExcel::commonGetExcelFile($file_name);
			self::sendEmail($file_info, $data['file_name']);
			$excel = \PhpOffice\PhpSpreadsheet\IOFactory::load($file_info['file']);

			$excel->setActiveSheetIndex(0);
			$sheet = $excel->getActiveSheet();

			$array = [];
			$contractor = [];
			foreach ($sheet->getRowIterator() as $row) {
				$arr = [];
				$cellIterator = $row->getCellIterator();
				foreach ($cellIterator as $cell) {
					$row = $cell->getRow();
					$coll = $cell->getColumn();
					$currentCell = $coll . $row;
					$value = trim($cell->getCalculatedValue());
					if (Date::isDateTime($cell)) {
						$value = date('d.m.Y', Date::excelToTimestamp($cell->getValue()));
					}
					if ($coll == 'A' && !$value) {
						/**
						 * Масив з пустими колонками
						 */
						break;
					} else if ($currentCell == 'A1' || $currentCell == 'A7' || $currentCell == 'A8') {
						/**
						 * Массив з Назвами колонок
						 */
						break;
					} else {
						if ($row == 2 || $row == 3 || $row == 4 || $row == 5 || $row == 6) {
							$contractor = CommonExcel::getContractorInfo($currentCell, $value, $contractor);
						} else {
							if (!$value) {
								$value = 'NULL';
							} else {
								switch ($names[$cell->getParent()->getCurrentColumn()]) {
									case 'animalType':
										$count = count($animalType);
										for ($i = 0; $i < $count; $i++) {
											if ($animalType[$i]['name'] == $value) {
												$value = $animalType[$i]['id'];
											}
										}
										break;
									case 'productType':
									case 'notesProduct':
										$count = count($productType);
										for ($i = 0; $i < $count; $i++) {
											if ($productType[$i]['name'] == $value) {
												$value = $productType[$i]['id'];
											}
										}
										break;
									case 'odMeasurement':
										$count = count($measureUnits);
										for ($i = 0; $i < $count; $i++) {
											if ($measureUnits[$i]['smallname'] == $value) {
												$value = $measureUnits[$i]['id'];
											}
										}
										break;
									case 'count':
										$value = (int)$value;
										break;
									case 'product':
										break;
								}
							}
							$arr[$names[$cell->getParent()->getCurrentColumn()]] = $value;
						}
					}
				}
				if ($arr) {
					$arr += [
						'ageGroup' => 'NULL',
						'animalBreed' => 'NULL',
						'feature' => 'NULL',
						'price' => 'NULL',
					];
					$array[] = $arr;
				}
			}
			$contractor['userId'] = $data['telegramId'];
			$contractor['fileName'] = $file_name;
			$contractor['fileSize'] = $file_info['size'];
			array_unshift($array, $contractor);

			$result = LoadDataFromUser::loadDataFromUser(json_encode($array, JSON_UNESCAPED_UNICODE), Volonter);
			if ($result[0]['status'] == 'success') {
				$text = 'Ваш документ оброблений.';
			} else {
				$text = 'Помилка обробки вашого документу.';
			}
		} catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
			$text = 'Модуль на технічній підтриці. Спробуйте пізніше.';
		}
		$telegram->sendMessage([
			'chat_id' => $data['telegramId'],
			'text' => $text,
		]);

	}

	/**
	 * @param $file_info
	 * @param $file_name
	 * @return void
	 */
	public static function sendEmail($file_info, $file_name){
		$message = array(
			'html' => '<p>Доброго дня!</p>',
			'text' => 'Доброго дня!',
			'subject' => 'До письма прикріплено файл-заявка на благодійність',
			'attachments' => array(
				$file_name => file_get_contents($file_info['file']),
			),
		);
		(new Mail())->mailTo($message);
	}

}
