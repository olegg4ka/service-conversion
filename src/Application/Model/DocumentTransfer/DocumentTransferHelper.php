<?php

namespace Application\Model\DocumentTransfer;


use Application\Model\Common\Documents\Excel\CommonExcel;
use Application\Model\DocumentTransfer\DataBaseRequest\LoadDataFromUser;
use Core\Backend\Common\Documents\SaveFiles\SaveFiles;
use Core\Backend\Component\Messenger\Telegram;
use Core\Component\Redis\Storage\Catalog;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use PhpOffice\PhpSpreadsheet\Shared\Date;

const Helper = 0;

/**
 *
 */
class DocumentTransferHelper
{
	/**
	 * @param array $data
	 * @return void
	 * @throws GuzzleException
	 * @throws Exception
	 */
	public static function saveFiles(array $data)
	{
		$file_name = SaveFiles::saveFiles($data);
		self::parseExcel($data, $file_name);
	}

	/**
	 * @param $data
	 * @param $file_name
	 * @return void
	 * @throws Exception
	 */
	public static function parseExcel($data, $file_name)
	{
		$text = 'Дякуємо, що скористалися ботом!';
		$telegram = (new Telegram())->getTelegram();
		try {
			$catalog = (new Catalog());
			$animalGroup = $catalog->getCatalog('AnimalGroup');
			$animalType = $catalog->getCatalog('AnimalType');
			$breed = $catalog->getCatalog('Breed');
			$productPurpose = $catalog->getCatalog('ProductPurpose');
			$productType = $catalog->getCatalog('ProductType');
			$measureUnits = $catalog->getCatalog('MeasureUnits');

			$names = [
				'A' => 'product',
				'B' => 'animalType',
				'C' => 'productType',
				'D' => 'notesProduct',
				'E' => 'ageGroup',
				'F' => 'animalBreed',
				'G' => 'feature',
				'H' => 'price',
				'I' => 'odMeasurement',
				'J' => 'count',
			];
			$file_info = CommonExcel::commonGetExcelFile($file_name);

			$excel = \PhpOffice\PhpSpreadsheet\IOFactory::load($file_info['file']);

			$excel->setActiveSheetIndex(0);
			$sheet = $excel->getActiveSheet();

			$array = [];
			$contractor = [];
			foreach ($sheet->getRowIterator() as $row) {
				$arr = [];
				$cellIterator = $row->getCellIterator();
				foreach ($cellIterator as $cell) {
					$row = $cell->getRow();
					$coll = $cell->getColumn();
					$currentCell = $coll . $row;
					$value = trim($cell->getCalculatedValue());
					if (Date::isDateTime($cell)) {
						$value = date('d.m.Y', Date::excelToTimestamp($cell->getValue()));
					}
					if ($coll == 'A' && !$value) {
						/**
						 * Масив з пустими колонками
						 */
						break;
					} else if ($currentCell == 'A1' || $currentCell == 'A6' || $currentCell == 'A7') {
						/**
						 * Массив з Назвами колонок
						 */
						break;
					} else {
						if ($row == 2 || $row == 3 || $row == 4 || $row == 5) {
							$contractor = CommonExcel::getContractorInfo($currentCell, $value, $contractor);
						} else {
							if (!$value) {
								$value = 'NULL';
							} else {
								switch ($names[$cell->getParent()->getCurrentColumn()]) {
									case 'animalType':
										$count = count($animalType);
										for ($i = 0; $i < $count; $i++) {
											if ($animalType[$i]['name'] == $value) {
												$value = $animalType[$i]['id'];
											}
										}
										break;
									case 'productType':
									case 'notesProduct':
										$count = count($productType);
										for ($i = 0; $i < $count; $i++) {
											if ($productType[$i]['name'] == $value) {
												$value = $productType[$i]['id'];
											}
										}
										break;
									case 'ageGroup':
										$count = count($animalGroup);
										for ($i = 0; $i < $count; $i++) {
											if ($animalGroup[$i]['name'] == $value) {
												$value = $animalGroup[$i]['id'];
											}
										}
										break;
									case 'animalBreed':
										$count = count($breed);
										for ($i = 0; $i < $count; $i++) {
											if ($breed[$i]['name'] == $value) {
												$value = $breed[$i]['id'];
											}
										}
										break;
									case 'feature':
										$count = count($productPurpose);
										for ($i = 0; $i < $count; $i++) {
											if ($productPurpose[$i]['name'] == $value) {
												$value = $productPurpose[$i]['id'];
											}
										}
										break;
									case 'odMeasurement':
										$count = count($measureUnits);
										for ($i = 0; $i < $count; $i++) {
											if ($measureUnits[$i]['smallname'] == $value) {
												$value = $measureUnits[$i]['id'];
											}
										}
										break;
									case 'count':
										$value = (int)$value;
										break;
									case 'price':
										$value = (int)$value * 100;
										break;
								}
							}
							$arr[$names[$cell->getParent()->getCurrentColumn()]] = $value;
						}
					}
				}
				if ($arr) {
					$array[] = $arr;
				}
			}
			$contractor['userId'] = $data['telegramId'];
			$contractor['contractorType'] = 'NULL';
			$contractor['fileName'] = $file_name;
			$contractor['fileSize'] = $file_info['size'];
			array_unshift($array, $contractor);
			$result = LoadDataFromUser::loadDataFromUser(json_encode($array, JSON_UNESCAPED_UNICODE), Helper);

			if ($result[0]['status'] == 'success') {
				$text = 'Ваш документ оброблений.';
			}else{
				$text = 'Помилка обробки вашого документу.';
			}
		} catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
			$text = 'Модуль на технічній підтриці. Спробуйте пізніше.';
		}
		$telegram->sendMessage([
			'chat_id' => $data['telegramId'],
			'text' => $text,
		]);
	}

}