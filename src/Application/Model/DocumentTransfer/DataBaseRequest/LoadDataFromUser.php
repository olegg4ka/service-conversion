<?php

namespace Application\Model\DocumentTransfer\DataBaseRequest;

use Core\Component\Container\Container;
use Core\Component\DataBase\Executor;

/**
 * Запит на збереження даних з екселю
 */
class LoadDataFromUser
{
	/**
	 * @param string $data
	 * @param int $isDeclaration
	 * @return array
	 */
	public static function loadDataFromUser(string $data, int $isDeclaration): array
	{
		$execute = Container::get(Executor::class);
		return $execute->execute("CALL loadDataFromUser(:isDeclaration, :data)",
			[
				':data' => $data,
				':isDeclaration' => $isDeclaration
			],
			2,
			'default_db'
		);
	}

}
