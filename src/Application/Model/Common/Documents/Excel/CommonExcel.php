<?php

namespace Application\Model\Common\Documents\Excel;


use Core\Backend\Component\MinIo\MinIo;
use Core\Component\Container\Container;
use Core\Component\Redis\Storage\Catalog;
use Exception;

/**
 * Загальний клас для виконання дій з Екселем
 */
class CommonExcel
{
	/**
	 * @param $currentCell
	 * @param $value
	 * @param $contractor
	 * @return mixed
	 * @throws Exception
	 */
	public static function getContractorInfo($currentCell, $value, $contractor)
	{
		$contractorType = (new Catalog())->getCatalog('ContractorType');
		if ($currentCell == 'B2') {
			$contractor['edrpou'] = $value;
		} else if ($currentCell == 'B3') {
			$contractor['organisation'] = $value;
		} else if ($currentCell == 'B4') {
			$contractor['contacts'] = $value;
			$contractor['address'] = $value;
		} else if ($currentCell == 'B5') {
			$contractor['telephone'] = $value;
		} else if ($currentCell == 'B6') {
			$count = count($contractorType);
			for ($i = 0; $i < $count; $i++) {
				if ($contractorType[$i]['name'] == $value) {
					$value = $contractorType[$i]['id'];
				}
			}
			$contractor['contractorType'] = $value;
		}
		return $contractor;
	}

	/**
	 * @param $file_name
	 * @return array
	 */
	public static function commonGetExcelFile($file_name): array
	{
		$minIo = new MinIo();
		$minIo->getFile(Container::get('config')['fileserver']['telegram']['bucketName'], $file_name);
		$file = '../data/files/' . $file_name;
		$size = $minIo->getFileSize();
		return [
			'size' => $size,
			'file' => $file
		];
	}

}
