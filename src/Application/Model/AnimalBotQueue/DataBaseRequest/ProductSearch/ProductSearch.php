<?php

namespace Application\Model\AnimalBotQueue\DataBaseRequest\ProductSearch;

use Core\Component\Container\Container;
use Core\Component\DataBase\Executor;

/**
 *
 */
class ProductSearch
{
	/**
	 * @param array $data
	 * @return array
	 */
	public static function getProductQueueSearch(array $data): array
	{
		$execute = Container::get(Executor::class);
		return $execute->execute("CALL createPDF(:id)",
			[
				':id' => $data['recordID']
			],
			2,
			'default_db'
		);
	}
}