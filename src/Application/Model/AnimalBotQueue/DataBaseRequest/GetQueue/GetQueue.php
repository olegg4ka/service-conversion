<?php

namespace Application\Model\AnimalBotQueue\DataBaseRequest\GetQueue;


use Core\Component\Container\Container;
use Core\Component\DataBase\Executor;

/**
 *
 */
class GetQueue
{
	/**
	 * @param array $data
	 * @return array
	 */
	public static function getQueueSearch($date): array
	{
		$execute = Container::get(Executor::class);
		return $execute->execute("CALL getQueue(:date)",
			[
				':date' => $date
			],
			2,
			'default_db'
		);
	}
}