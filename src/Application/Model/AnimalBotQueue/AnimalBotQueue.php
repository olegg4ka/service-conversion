<?php

namespace Application\Model\AnimalBotQueue;

use Application\Model\AnimalBotQueue\DataBaseRequest\GetQueue\GetQueue;
use Application\Model\AnimalBotQueue\DataBaseRequest\ProductSearch\ProductSearch;
use Core\Backend\Component\Messenger\Telegram;
use Core\Backend\Component\Prints\Prints;
use Core\Backend\Component\Twig\Twig;
use Telegram\Bot\FileUpload\InputFile;

/**
 *
 */
class AnimalBotQueue
{
	/**
	 * @param array $data
	 * @return void
	 */
	public static function getResult(array $data)
	{
		try {
			$telegram = (new Telegram())->getTelegram();
			$userQueue = GetQueue::getQueueSearch(date('d.m.yy'));
			$result = ProductSearch::getProductQueueSearch($data);
			$twig = (new Twig())->getTwig(['data' => $result], 'animalPrintForm.twig');
			$print = (new Prints());
			$print->getPdf($twig, ['orientation' => 'Landscape']);
			$content = (new InputFile())->setFile($print->getUrlFile())->setFilename($data['telegramId'] . '.pdf');
			$telegram->sendDocument([
				'chat_id' => $data['telegramId'],
				'document' => $content,
			]);
		} catch (\Exception $e) {
			dd($e);
		}
	}

}
