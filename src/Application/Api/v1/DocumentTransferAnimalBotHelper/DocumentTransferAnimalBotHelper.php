<?php

namespace Application\Api\v1\DocumentTransferAnimalBotHelper;

use Application\Model\DocumentTransfer\DocumentTransferHelper;
use Core\Backend\Component\User\UserInterface;
use Core\Component\Api\Api;
use Core\Component\Api\ApiFactory;
use Core\Component\Container\Container;
use Core\Component\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 *
 */
class DocumentTransferAnimalBotHelper implements Api
{
	/**
	 * @return string
	 * @throws GuzzleException
	 */
	public function execute(): string
	{
		$request = (new Request())->getRequest()->getContent();
		$req = (new ApiFactory())->rebuildResultFromApi($request);
		Container::get(UserInterface::class)->setUserID($req['telegramId']);
		DocumentTransferHelper::saveFiles($req);
		return (new JsonResponse(
			[
				'status' => 'success',
				'data' => $req
			],
			200, [], false))->send();
	}
}