<?php

namespace Application\Api\v1\QueueConvertion;


use Application\Model\AnimalBotQueue\AnimalBotQueue;
use Core\Backend\Component\User\UserInterface;
use Core\Component\Api\Api;
use Core\Component\Api\ApiFactory;
use Core\Component\Container\Container;
use Core\Component\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 *
 */
class QueueConvertion implements Api
{
	/**
	 * @return string
	 */
	public function execute(): string
	{
		$request = (new Request())->getRequest()->getContent();
		$req = (new ApiFactory())->rebuildResultFromApi($request);
		Container::get(UserInterface::class)->setUserID($req['telegramId']);
		AnimalBotQueue::getResult(json_decode($req['content'], true)[0]);
		return (new JsonResponse(
			[
				'status' => 'success',
			],
			200, [], false))->send();
	}

}